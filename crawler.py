from bs4 import BeautifulSoup, element
from urlparse import urlparse
import sys
from urllib2 import urlopen
from tldextract import extract
import logging
import re
import unicodedata
import random
import time


logging.basicConfig(filename='error.log',level=logging.DEBUG)


class crawler:
	def traverse_body(self,body):
		if isinstance(body,element.NavigableString) and not isinstance(body,element.Comment) and body.parent.name != 'script' and body.parent.name != 'style':
			return unicodedata.normalize('NFKD',body).encode('ascii','ignore')
		elif isinstance(body,element.Tag):
			self.body_content_list.extend(body.contents)
			return ''
		else:
			return ''

	
	def data_collect(self, domain, URL):
		print URL
		webpage_extensions = ('.html','.php','.htm','.do','.asp','.aspx','.axd','.asx','.do','.jsp','.pl', '.shtml','.js','.rhtml','.xhtml','.jspx','.xml','.rss','.svg','.jhtml','.asmx','.ashx','.action','.py','.php4','.php3','.phtml','.cgi','.cfm','.yaws')
		raw = None
		try:
			raw = urlopen(URL)
		except Exception:
			sys.stderr.write('The URL: '+URL+' cannot be opened\n')
			return None
	
		HTMLtext = raw.read()
		try:
			import lxml
			soup = BeautifulSoup(HTMLtext, 'lxml')
		except ImportError:
			soup = BeautifulSoup(HTMLtext, 'html.parser')
		except Exception:
			sys.stderr.write('The page cannot be parsed\n')
			return None
		

		meta_description_tag = soup.find(attrs={'name':'description'})
		if not meta_description_tag:
			meta_description_tag = soup.find(attrs={'name':'Description'})

		title = ''	
		if soup.title:
			if soup.title.string:
				title = soup.title.string
			if isinstance(title,unicode) and title != '':
				title = unicodedata.normalize('NFKD',title).encode('ascii','ignore')
		else:
			title = ''

		description_content = ''
		if meta_description_tag:
			if getattr(meta_description_tag, 'content', None):
				description_content = meta_description_tag['content']
			if isinstance(description_content,unicode) and description_content != '':
                                description_content = unicodedata.normalize('NFKD',description_content).encode('ascii','ignore')
		
		body_text = ''
		num = 0
		if soup.find('body'):
			self.body_content_list = [soup.find('body')]
			num = len(self.body_content_list)

		while num > 0:
			body = self.body_content_list[0]
			if body_text == '':
				body_text = self.traverse_body(body)
			else:
				body_text+= ' ' + self.traverse_body(body)
			self.body_content_list.pop(0)
			num = len(self.body_content_list)
		
		if title != '':
			title = title.replace("\t","")
			title = title.replace("\r","")
			title = title.replace("\n","")
		
		if description_content != '':
			description_content = description_content.replace("\t","")
			description_content = description_content.replace("\r","")
			description_content = description_content.replace("\n","")
		
		if body_text != '':
			body_text = body_text.replace("\t","")
			body_text = body_text.replace("\r","")
			body_text = body_text.replace("\n","")	

		data_list = [URL, title, description_content, body_text]
		self.pages.append(data_list)

		for link in soup.find_all('a', href=True):
			url = link['href']
			parsed_url = extract(url)
			if url.find('/') == 0:
				parse_curr_url = extract(URL)
				if parse_curr_url[0] == '':
					url = 'http://'+'.'.join(parse_curr_url[1:3])+url
				else:
					url = 'http://'+'.'.join(parse_curr_url[:3])+url
				if not (url in self.crawled_URLs or url in self.URL_list):
					if re.search('(\..?.?.?.?.?.?)$', url):
						if url.lower().endswith(webpage_extensions):
							if self.ori_path != '':
								if urlparse(url).path.find(self.ori_path) == 0:
									self.URL_list.append(url)
							else:
								self.URL_list.append(url)
					else:
						if self.ori_path != '':
							if urlparse(url).path.find(self.ori_path) == 0:
								self.URL_list.append(url)
						else:
							self.URL_list.append(url)

			elif '.'.join(parsed_url[1:3]) == domain and not (url in self.crawled_URLs or url in self.URL_list):
					if re.search('(\..?.?.?.?.?.?)$', url):
						if url.lower().endswith(webpage_extensions):
							if self.ori_path != '':
								if urlparse(url).path.find(self.ori_path) == 0:
									self.URL_list.append(url)
							else:
								self.URL_list.append(url)
					else:
						if self.ori_path != '':
							if urlparse(url).path.find(self.ori_path) == 0:
								self.URL_list.append(url)
						else:
							self.URL_list.append(url)

	def crawl(self, delay):
		crawl_no = len(self.URL_list)
		random_refresh = 0
		while crawl_no > 0:
			to_crawl = self.URL_list[-1]
			self.URL_list.pop()
			self.crawled_URLs[to_crawl] = None
			self.data_collect(self.domain, to_crawl)
			crawl_no = len(self.URL_list)
			random_refresh+= 1
			if random_refresh == 5 and delay:
				random_refresh = 1
				random.seed()
				time.sleep(random.random()*10)

	def __init__(self, domain, start_url):
		self.domain = domain
		if start_url.find('http', 0, len(start_url)) == -1 and start_url.find('http', 0, len(start_url)) != 0:
			start_url = 'http://'+start_url
		
		self.ori_path = urlparse(start_url).path
		self.crawled_URLs = dict()
		self.URL_list = [start_url]
		self.pages = []
		self.body_content_list = []
		self.total_length_list = dict()

	def __str__(self):
		string_result = ''
		for page in self.pages:
			count = 0
			for content in page:
				if count == 0:
					string_result+= 'URL: ' + content + '\n'
				elif count == 1:
					string_result+= 'Title: ' + content + '\n'
				elif count == 2:
					string_result+= 'Description: ' + content + '\n'
				else:
					string_result+= 'Body: ' + content + '\n'
				count+=1
		
		return string_result

def check_correct(domain,start_url):
	if '.'.join(extract(start_url)[1:3]) != domain:
		return False
	else:
		return True

if __name__ == "__main__":
	delay = sys.argv[1]
	if delay == 'yes':
		delay = True
	else:
		delay = False

	domain = sys.argv[2]
	start_url = domain
	filename = 'result.txt'
	if len(sys.argv) > 3:
		start_url = sys.argv[3]
	
	if check_correct(domain,start_url):	
		if len(sys.argv) > 4:
			filename = sys.argv[4]

		crawlerBOT = crawler(domain,start_url)
		crawlerBOT.crawl(delay)
		writeto = open(filename, 'w')
		writeto.write(str(crawlerBOT))
		writeto.close()
	else:
		sys.stderr.write("Given URL does not have correct domain name\n")
