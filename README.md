Module Installations:
=====================

This project uses two external modules and uses the lxml parser to parse HTML instead of
the built-in HTMLParser in the case that it is available.

Installing BeautifulSoup:

	pip install beautifulsoup4

Installing tldextract:

	pip install tldextract

Installing lxml:

On Ubuntu you would need to install the Python header files first. You can do this by doing

	sudo apt-get install python-dev

Then you would need to either manually install libxml2-dev and libxslt-dev by doing

	sudo apt-get install libxml2-dev libxslt-dev

or you can do

	STATIC_DEPS=true sudo pip install lxml

which will automatically install the two libraries (requires Internet connection).

To install on a Mac, do

	STATIC_DEPS=true sudo pip install lxml

In the case that lxml cannot be installed correctly, HTMLParser would instead be used

Using the Modules:
==================

The following explains how to use the modules in this project:

To use crawler.py run it as the following:

	python crawler.py delay domain startURL filename

If **delay** is 'yes' then the crawler will delay every 5 crawls with a random interval between 0 to 10
seconds. Anything other than that is considered as no delay

**domain** refers to the domain name of the website to be crawled and is a necessary
argument to be given. **startURL** is an optional parameter and it should be given when the start of the
crawl i.e the beginning URL to be crawled, wants to be explicitly stated. If **startURL** is not given,
by default the program will crawl to

	http://*domain name given as arg1*

Ensure that the given URL in **startURL** has the correct domain name. **filename** is an optional parameter
if the user wants to specify a filename in which the result of the crawl will be saved to. If not
given by default the result would be stored in a file called 'result.txt'

---------------------------------------------------------------------------------------------------

allinfo.py should never be run individually. keyMatch.py should be run only when a crawl result
has been made. To run do the following:

	python keyMatch.py arg1 arg2

arg1 specifies the name of the file in which the result of the crawl is stored at and is a required
parameter. keyMatch.py excepts the keywords from stdin, as such either the contents of a file is
redirected or the user manually gives the keywords.

arg2 (optional) specifies the name of the file in which the result of the matching algorithm will 
be saved to. If not specified by default the result is saved to a file called 'qmatch.txt'

Potential Errors:
=================

Because the module *tldextract* is rather shifty, some errors with it might occur. Current errors found:

- IOError: [Errno 13] Permission denied: '/usr/local/lib/python2.7/dist-packages/oauth2-1.5.211.egg-info/top\_level.txt'. Simply do _sudo python crawler.py_ instead.
