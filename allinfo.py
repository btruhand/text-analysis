
class info:
	def build_dictionary(self):
		iteration = 0
		for content in self.tuple_info:
			count = 0
			occurence_per_doc = dict()
			for word in content.split()[1:]:
				lower_word = word.lower()
				if lower_word in occurence_per_doc:
					occurence_per_doc[lower_word]+= 1
				else:
					occurence_per_doc[lower_word] = 1
				count+=1
			
			if iteration == 0:
				self.occurence_dict["Title"] = occurence_per_doc
				self.length_per_content["Title"] = count
				iteration+=1
			elif iteration == 1:
				self.occurence_dict["Desc"] = occurence_per_doc
				self.length_per_content["Desc"] = count
				iteration+=1
			else:
				self.occurence_dict["Body"] = occurence_per_doc
				self.length_per_content["Body"] = count
				
	def __init__(self, tuple_info):
		self.tuple_info = tuple_info
		self.length_per_content = dict()
		self.occurence_dict = dict()
		self.build_dictionary()

	def get_doclength(self, which_doc):
		return self.length_per_content[which_doc]

	def get_occurinfo(self, which_doc):
		return self.occurence_dict[which_doc]	

	def get_url(self):
		return self.tuple_info[0].split()[1]

class allpages:
	def make_non_empty(self):
		for doc in ["Title","Desc","Body"]:
			self.non_empty_docs[doc] = 0
			for info in self.get_infos():
				if info.get_doclength(doc) != 0:
					self.non_empty_docs[doc]+= 1

	def get_non_empty_docs(self, which_doc):
		return self.non_empty_docs[which_doc]

	def get_infos(self):
		return self.info_list

	def make_total_length(self):
		for doc in ["Title", "Desc", "Body"]:
			total_length = 0
			for info in self.get_infos():
				total_length+= info.get_doclength(doc)
			self.totdoclength[doc] = total_length

	def get_totdoclength(self, which_doc):
		return self.totdoclength[which_doc]

	def __init__(self, list_of_pages):
		self.info_list = [info(page) for page in list_of_pages]
		self.totdoclength = dict()
		self.non_empty_docs = dict()
		self.make_non_empty()
		self.make_total_length()
