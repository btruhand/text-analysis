import math
import sys
import allinfo
import textwrap

def check_occurence(info,check,which_doc,presence=True):
	if check in info.get_occurinfo(which_doc):
		if not presence:
			return info.get_occurinfo(which_doc)[check]
		else:
			return True
	else:
		if not presence:
			return 0
		else:
			return False

def IDF(rank_pages,query):
	documents = ["Title", "Desc", "Body"]
	keywords_lower = [key.lower() for key in query.split()]

	keyoccurence = dict()	
	
	for keyword in keywords_lower:
		docdict = dict()
		for doc in documents:
			for info in rank_pages.get_infos():
				if check_occurence(info,keyword,doc):
					if doc in docdict.keys():
						docdict[doc]+= 1
					else:
						docdict[doc] = 1
				else:
					if not doc in docdict.keys():
						docdict[doc] = 0
		keyoccurence[keyword] = docdict	

	IDFscores = dict()
	for key in keyoccurence.keys():
		docIDF = dict()
		for doc in documents:
			IDFscore = math.log((rank_pages.get_non_empty_docs(doc)+1)/(keyoccurence[key][doc]+0.5))
			docIDF[doc] = IDFscore
		IDFscores[key] = docIDF
	
	return IDFscores
		
			
def BM25_scoring(rank_pages, query):
	keywords_lower = [key.lower() for key in query.split()]
	documents = ["Title","Desc","Body"]
	
	IDFscores = IDF(rank_pages, query)
	BM25ratings = []
	maximumScore = 0.0
	for info in rank_pages.get_infos():
		BM25score = 0
		for doc in documents:
			for key in keywords_lower:
				if doc == "Title":
					param = 0.95
				elif doc == "Desc":
					param = 0.95
				else:
					param = 0.6
				numerator = check_occurence(info,key,doc,False)*(2+1)
				if rank_pages.get_non_empty_docs(doc) != 0:
					average_length = rank_pages.get_totdoclength(doc)/rank_pages.get_non_empty_docs(doc)
				else:
					average_length = 0	
				denominator = check_occurence(info,key,doc,False)+2*(1-param+param*(info.get_doclength(doc)/average_length))
				BM25score+=IDFscores[key][doc]*(numerator/denominator)
		
		if BM25score > maximumScore:
			maximumScore = BM25score

		BM25score_tuple = (info.get_url(),BM25score)
		if len(BM25ratings) < 5:
			BM25ratings.append(BM25score_tuple)
		else:
			for pos in range(0,5):
				if BM25score > BM25ratings[pos][1]:
					tmp = BM25score_tuple
					BM25score_tuple = BM25ratings[pos]
					BM25ratings[pos] = tmp
	
	sorted(BM25ratings, key = lambda BM25: BM25[1])
	query_result = query
	if maximumScore != 0.0:
		for rating in BM25ratings:
			query_result+= ';' + rating[0]
		
		print query_result
	else:
		query_result+= ';' + "No match found"

		print query_result

if __name__ == "__main__":
	filename = sys.argv[1]
	save_to = 'qmatch.txt'

	crawl_result_file = open(filename, 'r')
	list_of_pages = []
	page_content = []
	every_four_lines = 0
	for line in crawl_result_file:
		page_content.append(line)
		every_four_lines+= 1
		if every_four_lines == 4:
			list_of_pages.append(page_content)
			page_content = []
			every_four_lines = 0
	fullinfo = allinfo.allpages(list_of_pages)

	print 'Query;URL 1;URL 2;URL 3;URL 4;URL 5'
	
	for query in sys.stdin:
		BM25_scoring(fullinfo, query)
